package com.alex.asteroids.client.logic

import java.util.*

object GameStrategy {

    fun nextMove() {
        if (canMove()) println(GameLogic.go())
        else changeDirection()
    }

    private fun canMove() = !(GameLogic.checkWall() || GameLogic.checkAsteroid())

    private fun changeDirection() {
        val random = Random()
        val randomInt = random.nextInt(4)
        when (randomInt) {
            0 -> GameLogic.up()
            1 -> GameLogic.down()
            2 -> GameLogic.left()
            3 -> GameLogic.right()
        }
    }
}