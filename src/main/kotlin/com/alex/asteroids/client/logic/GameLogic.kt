package com.alex.asteroids.client.logic

import com.alex.asteroids.client.messagessevice.MessagesReceiver
import com.alex.asteroids.client.reader
import com.alex.asteroids.client.writer

object GameLogic {

    private val FINISHED_MESSAGE = "Game finished"

    fun go() = sendCommand("go")

    fun up(): Boolean {
        val answer = sendCommand("up")
        return isAnswerSuccess(answer)
    }

    fun down(): Boolean {
        val answer = sendCommand("down")
        return isAnswerSuccess(answer)
    }

    fun left(): Boolean {
        val answer = sendCommand("left")
        return isAnswerSuccess(answer)
    }

    fun right(): Boolean {
        val answer = sendCommand("right")
        return isAnswerSuccess(answer)
    }

    fun checkWall(): Boolean {
        val answer = sendCommand("isWall")
        return isAnswerSuccess(answer)
    }

    fun checkAsteroid(): Boolean {
        val answer = sendCommand("isAsteroid")
        return isAnswerSuccess(answer)
    }

    private fun isAnswerSuccess(answer: String) =
            answer == "success" || answer == "t"


    private fun sendCommand(command: String): String {
        writer.println(command)
        val answer = reader.readLine()
        if (answer == "finish") {
            MessagesReceiver.changeReadingState()
            throw Exception(FINISHED_MESSAGE)
        }
        return answer
    }
}