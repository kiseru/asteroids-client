package com.alex.asteroids.client

import com.alex.asteroids.client.logic.GameStrategy
import com.alex.asteroids.client.messagessevice.MessagesReceiver

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket
import java.util.*

lateinit var reader: BufferedReader
lateinit var writer: PrintWriter

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    print("Print a host: ")
    val host = scanner.next()
    print("Print a port: ")
    val port = scanner.nextInt()

    val socket = Socket(host, port)
    reader = BufferedReader(InputStreamReader(socket.getInputStream()))
    writer = PrintWriter(socket.getOutputStream(), true)

    MessagesReceiver.start()

    writer.println(scanner.next())
    while (MessagesReceiver.readingNeeded) Thread.sleep(500)
    while (true) {
        try {
            GameStrategy.nextMove()
        } catch (e: Exception) {
            break
        }
    }
}

