package com.alex.asteroids.client.messagessevice

import com.alex.asteroids.client.reader

object MessagesReceiver : Thread() {

    var readingNeeded = true
    private set

    fun changeReadingState() {
        readingNeeded = !readingNeeded
    }

    override fun run() {
        var inputText: String? = reader.readLine()
        while (inputText != null) {
            if (inputText == "start") {
                println("Game is started")
                changeReadingState()
                while (!MessagesReceiver.readingNeeded) Thread.sleep(5000)
            } else println(inputText)
            inputText = reader.readLine()
        }
    }
}
