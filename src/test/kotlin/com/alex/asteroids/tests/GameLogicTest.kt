package com.alex.asteroids.tests

import com.alex.asteroids.client.logic.GameLogic
import com.alex.asteroids.client.reader
import com.alex.asteroids.client.writer
import org.junit.Test
import org.junit.Assert.assertEquals
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import java.io.BufferedReader
import java.io.PrintWriter
import kotlin.reflect.full.functions
import kotlin.test.assertTrue
import kotlin.test.fail

class GameLogicTest {

    private val testReader = mock(BufferedReader::class.java)
    private val testWriter = mock(PrintWriter::class.java)

    init {
        reader = testReader
        writer = testWriter
    }

    @Test
    fun goMustReturnScore() {
        val expectedValue = "120"
        `when`(testReader.readLine()).thenReturn(expectedValue)
        val actualValue = GameLogic.go()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun downMustReturnTrue() {
        val expectedValue = true
        `when`(testReader.readLine()).thenReturn("t")
        val actualValue = GameLogic.down()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun downMustReturnFalse() {
        val expectedValue = false
        `when`(testReader.readLine()).thenReturn("f")
        val actualValue = GameLogic.down()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun leftMustReturnTrue() {
        val expectedValue = true
        `when`(testReader.readLine()).thenReturn("t")
        val actualValue = GameLogic.left()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun leftMustReturnFalse() {
        val expectedValue = false
        `when`(testReader.readLine()).thenReturn("f")
        val actualValue = GameLogic.left()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun rightMustReturnTrue() {
        val expectedValue = true
        `when`(testReader.readLine()).thenReturn("t")
        val actualValue = GameLogic.right()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun rightMustReturnFalse() {
        val expectedValue = false
        `when`(testReader.readLine()).thenReturn("f")
        val actualValue = GameLogic.right()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun checkWallMustReturnTrue() {
        val expectedValue = true
        `when`(testReader.readLine()).thenReturn("t")
        val actualValue = GameLogic.checkWall()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun checkWallMustReturnFalse() {
        val expectedValue = false
        `when`(testReader.readLine()).thenReturn("f")
        val actualValue = GameLogic.checkWall()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun checkAsteroidMustReturnTrue() {
        val expectedValue = true
        `when`(testReader.readLine()).thenReturn("t")
        val actualValue = GameLogic.checkAsteroid()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun checkAsteroidMustReturnFalse() {
        val expectedValue = false
        `when`(testReader.readLine()).thenReturn("f")
        val actualValue = GameLogic.checkAsteroid()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun sendMessageMustThrowException() {
        val sendMessageMethod = GameLogic::class.functions
                .first { function -> function.name == "sendCommand" }
        `when`(testReader.readLine()).thenReturn("finish")
        try {
            sendMessageMethod.call()
            fail()
        } catch (e: Exception) {
            assertTrue(true)
        }
    }
}