FROM java:9-jdk

RUN mkdir /usr/src/asteroids-client
WORKDIR /usr/src/asteroids-client

COPY target/ClientAsteroids-1.0-jar-with-dependencies.jar /usr/src/asteroids-client
CMD ["java", "-jar", "ClientAsteroids-1.0-jar-with-dependencies.jar"]